from qgis.gui import (QgsMapToolDigitizeFeature)

class PolygonDrawTool(QgsMapToolDigitizeFeature):
    def __init__(self, canvas, cdw, capture_mode, layer):
        self.layer = layer
        self.capture_mode = capture_mode
        self.canvas = canvas
        self.cdw = cdw
        if self.layer:
            super(PolygonDrawTool, self).__init__(self.canvas, self.cdw, self.capture_mode)
    
    def addFeature(self, f):
        if self.layer:
            #Add step to undo history
            self.layer.beginEditCommand("Add feature")
            self.layer.addFeature(f)
            self.layer.triggerRepaint()
            self.layer.endEditCommand()
            return True
        else:
            return False
